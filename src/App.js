import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./components/login/login";
import CreateCompany from "./components/create-company/create-company";
import AddCameraSetup from "./components/AddCameraSetup/AddCameraSetup";
import SettingCamera from "./components/AddCameraSetup/SettingCamera";
import SettingCameraNext from "./components/AddCameraSetup/SettingCameraNext1";
import ActivityDetection from "./components/AddCameraSetup/ActivityDetection";
import SetupProcess from "./components/SetupProcess/SetupProcess";
import Recorders from "./components/Recorders/Recorders"
import AddRecorder from './components/AddRecorder/AddRecorder'
import RecorderTable from './components/AddRecorder/Table'
import NewAddCamera from './components/Recorders/NewCamera'
import ExistingUser from './components/AddCameraSetup/ExistingUser'
import AddYourTeamTable from './components/AddRecorder/AddYourTeamTable'
import CameraTable from './components/AddRecorder/CameraTable'
import AddYourTeam from "./components/Recorders/AddYourTeam"
import SetupCompleted from "./components/SetupProcess/SetupCompleted"
import NewUser from './components/AddCameraSetup/NewUser';



class App extends React.Component {
  render(){
  return (<Router>
    <div className="wrapper">
      <div className="wobot-wrapper">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/sign-in" component={Login} />
            <Route path="/create-company" component={CreateCompany} />
            <Route path="/add-camera-setup" component={AddCameraSetup} />
            <Route path="/setting-camera" component={SettingCamera} />
            <Route path="/setting-camera-next" component={SettingCameraNext} />
            <Route path="/activity-detection" component={ActivityDetection} />
            <Route path="/setup-process" component={SetupProcess} />
            <Route path="/recorders" component={Recorders} />
            <Route path="/add-recorders" component={AddRecorder} />
            <Route path="/recorders-table" component={RecorderTable} />
            <Route path="/add-new-camera" component={NewAddCamera} />
            <Route path="/existing-user" component={ExistingUser} />
            <Route path="/add-your-team-table" component={AddYourTeamTable} />
            <Route path="/cameras-table" component={CameraTable} />
            <Route path="/add-your-team" component={AddYourTeam} />
            <Route path="/setup-completed" component={SetupCompleted} />
            <Route path="/new-user" component={NewUser} />
          </Switch>
      </div>
    </div></Router>
  );
}
}

export default App;
