import React, { Component } from "react";
import { Link } from "react-router-dom";
import Footer from "../footer/footer";


export default class CreateCompany extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            name: ""
        };
    }

    handleEmailChange = evt => {
        this.setState({ email: evt.target.value });
    };

    handleNameChange = evt => {
        this.setState({ name: evt.target.value });
    };

    render() {
        const { email, name } = this.state;
        const isEnabled = email.length > 0 && name.length > 0;
        return (
            <div className="content">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <div className="heading"><h2>Create a Company</h2></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/setup-process">
                    <div className="form-group row">
                        <label className="col-md-4" for="cname">Company Name</label>
                        <input type="cname" id="cname" className="form-control col-md-6" />
                    </div>

                    <div className="form-group row">
                        <label className="col-md-4" for="yname">Your Name</label>
                        <input type="yname" id="yname" className="form-control col-md-6" value={this.state.name}
                            onChange={this.handleNameChange} />
                    </div>

                    <div className="form-group row">
                        <label className="col-md-4" for="email">Email</label>
                        <input type="email" id="email" className="form-control col-md-6" placeholder="signup@newcompany.com" value={this.state.email}
                            onChange={this.handleEmailChange} />
                    </div>
                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Skip entire setup
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/"}>
                                Back
                    </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next" disabled={!isEnabled}>Next</button>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
