import React from 'react';
import { Link } from "react-router-dom";
import Select from 'react-select';
import Popup from "reactjs-popup";
import Footer from "../footer/footer";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { TimePicker } from 'antd';
import "antd/dist/antd.css";
import 'mdbreact/dist/css/mdb.css';
//import DatePicker from 'react-date-picker';

import { 
    DatePicker,
 } from '@progress/kendo-react-dateinputs'

import '@progress/kendo-react-intl'
import '@progress/kendo-react-tooltip'
import '@progress/kendo-react-common'
import '@progress/kendo-react-popup'
import '@progress/kendo-react-buttons'
import '@progress/kendo-date-math'
import '@progress/kendo-react-dropdowns'



function onChange(time, timeString) {
    console.log(time, timeString);
}


const options = [
    '1', '2', '3'
];
const days = [
    { label: "Sunday", value: 1 },
    { label: "Monday", value: 2 },
    { label: "Tuesday", value: 3 },
]

class SettingCameraNext extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.state = {
            listitems: ["M", "T", "W", "T", "F", "S", "S"]        };

    }

    openModal() {
        this.setState({ open: true });
    }
    closeModal() {
        this.setState({ open: false });
    }

    defaultValue = new Date();


    render() {
        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="heading"><h2>Add & Setup Camera</h2></div>
                        </div>
                        <div className="col-md-6">
                            <div className="camera-settings">
                                <Link className="" to={"/setting-camera"}>
                                    Connect
                       </Link>
                                <Link className="active" to={"/add-camera-setup"}>
                                    Camera settings
                       </Link>
                            </div>
                        </div>
                    </div>
                    <div className="main-content">
                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link to={"/setting-camera-next"} className="active default-btn btn">Item detection</Link></li>
                                <li><button className="default-btn btn">Activity detection</button></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link className="active default-btn btn">Hairnet</Link></li>
                                <li><Link className="default-btn btn">Apron</Link></li>
                                <li><Link className="active default-btn btn">Gloves</Link></li>
                                <li><Link className="default-btn btn">Gloves</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Add Schedule</label>
                            <ul>
                                <li className="add-new btn" onClick={this.openModal}> Add </li>
                            </ul>
                            <span>Every 2 days on Thu at 6:30pm</span><span className="close"></span>
                        </div>


                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Setup to other cameras
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/setting-camera"}>
                                Cancel
                    </Link>
                            <a href="/add-camera-setup" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Save</a>
                        </div>
                    </div>

                </form>
                <Footer />
                <div className="calendar-popup-box">
                <Popup
                    open={this.state.open}
                    closeOnDocumentClick
                    onClose={this.closeModal}
                >
                    <div className="modal-content add-calendar">
                        {/* <span className="close" onClick={this.closeModal}> &times;</span> */}
                        <h2>Add Schedule</h2>
                        <div className="row">
                            <div className="col-md-5"><span className="repeat">Repeat every</span><Dropdown options={options} onChange={this._onSelect} placeholder="1" /></div>
                            <div className="col-md-4">
                                <Select options={days} placeholder="Days" />
                            </div>
                        </div>
                        <div className="row time-select">
                            <div className="col-md-6">
                                <label>At</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                            <div className="col-md-6">
                                <label>Till</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                        </div>
                        <div className="row time-select">
                            <div className="col-md-6">
                                <label>At</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                            <div className="col-md-6">
                                <label>Till</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                        </div>

                        <div className="row repeat-days">
                            <div className="col-md-3">Repeat on </div>
                            <div className="col-md-9">
                                <ul className="list-days">
                                    {this.state.listitems.map(listitem => (
                                        <li
                                            key={listitem}
                                            className=""
                                        >
                                            {listitem}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <span> Ends</span>
                                <form onSubmit={this.handleSubmit}>
                                    <ul className="radio-btn">
                                        <li>
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio" />
                                                Never
                                            </label>
                                        </li>

                                        <li>
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio"
                                                    checked />
                                                    On
                                                </label>

                                            <DatePicker
                                                defaultValue={this.defaultValue}
                                            />
                                        </li>
                                    </ul>

                                    <div className="btn-add-submit"> <a href="/activity-detection" type="submit" className="add-new btn">Add</a></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Popup>
                </div>
            </div>
        );
    }
}
export default SettingCameraNext;

