import React from 'react';
import { Link } from "react-router-dom";
import Select from 'react-select';
import Popup from "reactjs-popup";
import Footer from "../footer/footer";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { TimePicker } from 'antd';
import "antd/dist/antd.css";
import 'mdbreact/dist/css/mdb.css';
//import DatePicker from 'react-date-picker';
import Webcam from "react-webcam";
import ReactTooltip from "react-tooltip";
import SearchBar from 'material-ui-search-bar'

import { 
    DatePicker,
 } from '@progress/kendo-react-dateinputs'

import '@progress/kendo-react-intl'
import '@progress/kendo-react-tooltip'
import '@progress/kendo-react-common'
import '@progress/kendo-react-popup'
import '@progress/kendo-react-buttons'
import '@progress/kendo-date-math'
import '@progress/kendo-react-dropdowns'

var webcamRef;

const SelectList1 = [
    { label: "City 1", value: 1 },
    { label: "City 2", value: 2 },
    { label: "City 3", value: 3 },
    { label: "City 4", value: 4 },
]
const SelectList2 = [
    { label: "State 1", value: 1 },
    { label: "State 2", value: 2 },
    { label: "State 3", value: 3 },
    { label: "State 4", value: 4 },
]
const SelectList3 = [
    { label: "Action 1", value: 1 },
    { label: "Action 2", value: 2 },
    { label: "Action 3", value: 3 },
    { label: "Action 4", value: 4 },
]


function onChange(time, timeString) {
    console.log(time, timeString);
}


const options = [
    '1', '2', '3'
];

const days = [
    { label: "Sunday", value: 1 },
    { label: "Monday", value: 2 },
    { label: "Tuesday", value: 3 },
]

class ActivityDetection extends React.Component {

    constructor(props) {
        super(props);
        this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.state = { open2: false };
        this.openModal2 = this.openModal2.bind(this);
        this.closeModal2 = this.closeModal2.bind(this);

        this.state = { open3: false };
        this.openModal3 = this.openModal3.bind(this);
        this.closeModal3 = this.closeModal3.bind(this);
        this.state = {
            listitems: ["M", "T", "W", "T", "F", "S", "S"]
        };

    }

    openModal() {
        this.setState({ open: true });
    }
    closeModal() {
        this.setState({ open: false });
    }

    openModal2() {
        this.setState({ open2: true });
    }
    closeModal2() {
        this.setState({ open2: false });
    }

    openModal3() {
        this.setState({ open3: true });
    }
    closeModal3() {
        this.setState({ open3: false });
    }


    defaultValue = new Date();

    render() {

        const videoConstraints = {
            width: 240,
            height: 180,
            facingMode: "user"
        };


        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit}>
                    <div className="row">
                        <div className="col-md-6">
                            <div className="heading"><h2>Add & Setup Camera</h2></div>
                        </div>
                        <div className="col-md-6">
                            <div className="camera-settings">
                                <Link className="" to={"/setting-camera"}>
                                    Connect
                       </Link>
                                <Link className="active" to={"/add-camera-setup"}>
                                    Camera settings
                       </Link>
                            </div>
                        </div>
                    </div>
                    <div className="main-content">
                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link to={"/setting-camera-next"} className="default-btn btn">Item detection</Link></li>
                                <li><Link to={"/activity-detection"} className="active default-btn btn">Activity detection</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Select Model</label>
                            <ul>
                                <li><Link className="active default-btn btn">Hairnet</Link></li>
                                <li><Link className="default-btn btn">Apron</Link></li>
                                <li><Link className="active default-btn btn">Gloves</Link></li>
                                <li><Link className="default-btn btn">Gloves</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Add Schedule</label>
                            <ul>
                                <li className="add-new btn" onClick={this.openModal}> Add </li>
                            </ul>
                            <span>Every 2 days on Thu at 6:30pm</span><span className="close"></span>
                        </div>

                        <div className="region-interest">
                            <label>Region of interest</label>
                            <ul className="radio-btn">
                                <li>
                                    <label>
                                        <input
                                            type="radio"
                                            name="radio" />
                                                Automatic
                                            </label>
                                </li>
                                <li>
                                    <label>
                                        <input
                                            type="radio"
                                            name="radio" />
                                                Manual
                                            </label>
                                </li>

                            </ul>

                        </div>

                        <div className="live-camera-view">
                            <div className="row">
                                <div className="col-md-4">
                                    <span>Basin</span>
                                    <div className="view-part">
                                        <Webcam
                                            audio={false}
                                            height={180}
                                            ref={webcamRef}
                                            screenshotFormat="image/jpeg"
                                            width={240}
                                            videoConstraints={videoConstraints}
                                        />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <span>Hand</span>
                                    <div className="view-part">
                                        <Webcam
                                            audio={false}
                                            height={180}
                                            ref={webcamRef}
                                            screenshotFormat="image/jpeg"
                                            width={240}
                                            videoConstraints={videoConstraints}
                                        />
                                    </div>
                                </div>

                                <div className="col-md-4">
                                    <div className="face-modal">
                                        <span>Face</span>
                                        <span class="addmore" onClick={this.openModal2}> Add</span>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn" onClick={this.openModal3} to={"#"}>
                                Setup to other cameras
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/setting-camera"}>
                                Cancel
                    </Link>
                            <a href="/add-camera-setup" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Save</a>
                        </div>
                    </div>

                </form>
                <Footer />

                <div className="calendar-popup-box">
                <Popup
                    open={this.state.open}
                    closeOnDocumentClick
                    onClose={this.closeModal}
                >
                    <div className="modal-content add-calendar">
                        {/* <span className="close" onClick={this.closeModal}> &times;</span> */}
                        <h2>Add Schedule</h2>
                        <div className="row">
                            <div className="col-md-5"><span className="repeat">Repeat every</span><Dropdown options={options} onChange={this._onSelect} placeholder="1" /></div>
                            <div className="col-md-4">
                                <Select options={days} placeholder="Days" />
                            </div>
                        </div>
                        <div className="row time-select">
                            <div className="col-md-6">
                                <label>At</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                            <div className="col-md-6">
                                <label>Till</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                        </div>
                        <div className="row time-select">
                            <div className="col-md-6">
                                <label>At</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                            <div className="col-md-6">
                                <label>Till</label>
                                <TimePicker use12Hours onChange={onChange} placeholder="10:00 AM" />
                            </div>
                        </div>

                        <div className="row repeat-days">
                            <div className="col-md-3">Repeat on </div>
                            <div className="col-md-9">
                                <ul className="list-days">
                                    {this.state.listitems.map(listitem => (
                                        <li
                                            key={listitem}
                                            className=""
                                        >
                                            {listitem}
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-md-12">
                                <span> Ends</span>
                                <form onSubmit={this.handleSubmit}>
                                    <ul className="radio-btn">
                                        <li>
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio" />
                                                Never
                                            </label>
                                        </li>

                                        <li>
                                            <label>
                                                <input
                                                    type="radio"
                                                    name="radio"
                                                    checked />
                                                    On
                                                </label>

                                            <DatePicker
                                                defaultValue={this.defaultValue}
                                            />
                                        </li>
                                    </ul>

                                    <div className="btn-add-submit"> <a href="/activity-detection" type="submit" className="add-new btn">Add</a></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </Popup>
                </div>

                <div className="face-modal-content">
                    <Popup
                        open={this.state.open2}
                        closeOnDocumentClick
                        onClose={this.closeModal2}
                    >
                        <form onSubmit={this.handleSubmit} action="/activity-detection" className="modal-content add-calendar">
                            {/* <span className="close" onClick={this.closeModal}> &times;</span> */}
                            <h2>Add Region of Interest: Face <span class="skip-btn btn default-btn ">Refresh Image</span></h2>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="face-view-part">
                                        <Webcam
                                            audio={false}
                                            height={250}
                                            ref={webcamRef}
                                            screenshotFormat="image/jpeg"
                                            width={450}
                                            videoConstraints={videoConstraints}
                                        />
                                        <p> <span className="info-text" data-tip="Some info here">
                                            <img width="20" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/info.png`} alt="" />
                                        </span> <ReactTooltip />Drag and draw the region on the image that should be tracked </p>
                                    </div>
                                </div>
                            </div>
                            <div className="row bottom-btns">
                                <div className="col-md-12 text-right">
                                    <Link  onClick={this.closeModal2} className=" default-btn btn" to={"#"}>
                                        Cancel
                    </Link>
                                    <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Same</button>
                                </div>
                            </div>
                        </form>
                    </Popup>
                </div>

                <form onSubmit={this.handleSubmit} action="/activity-detection" className="copy-settings-camera">
                    <Popup
                        open={this.state.open3}
                        closeOnDocumentClick
                        onClose={this.closeModal3}
                    >
                        <div className="modal-content add-calendar">
                            {/* <span className="close" onClick={this.closeModal}> &times;</span> */}
                            <h2>Copy Settings</h2>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="location-box">
                                        <SearchBar
                                            onChange={() => console.log('onChange')}
                                            onRequestSearch={() => console.log('onRequestSearch')}
                                        />
                                    </div>
                                    <div className="camera-type select-list">
                                        <div className="select-box">
                                            <Select options={SelectList1} placeholder="City" />
                                        </div>
                                        <div className="select-box">
                                            <Select options={SelectList2} placeholder="State" />
                                        </div>
                                        <div className="select-box">
                                            <Select options={SelectList3} placeholder="Center" />
                                        </div>
                                    </div>
                                    <div class="camera-type select-list">
                                        <label>2 Cameras found</label>
                                        <ul>
                                            <li><span class="default-btn btn">Select all</span></li>
                                            <li><span class="default-btn btn">Select none</span></li>
                                        </ul>
                                    </div>

                                    <ul className="radio-btn">
                                        <li>
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    name="checkbox" checked />
                                                Camera 1
                                            </label>
                                        </li>
                                        <li>
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    name="checkbox" />
                                                Camera 2
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="row bottom-btns">
                                <div className="col-md-12 text-right">
                                    <Link onClick={this.closeModal3} className=" default-btn btn" to={"/activity-detection"}>
                                        Cancel
                            </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Copy</button>
                                </div>
                            </div>
                        </div>
                    </Popup>

                </form>
            </div>
        );
    }
}
export default ActivityDetection;


