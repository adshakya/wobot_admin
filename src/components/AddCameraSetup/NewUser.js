import React from 'react';
import Select from 'react-select';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";



const SelectList1 = [
    { label: "Admin", value: 1 },
    { label: "Custom", value: 2 },
]

class NewUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/setting-camera-next">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading"><h2>Add User</h2></div>
                            <div className="camera-type select-list select-modals">
                            <label>User Type</label>
                            <div className="select-box">
                            <ul>
                                <li><Link class="active default-btn btn" to={"/new-user"}>New User</Link></li>
                                <li><Link class="default-btn btn" to={"/existing-user"}>Existing User</Link></li>
                            </ul>
                         </div>
                        </div>
                        </div>

                    </div>
                    <div className="main-content add-user-list">
                    <div className="camera-type select-list">
                            <label>Email address</label>
                            <div className="select-box">
                                <input type="text" name="" placeholder="email@team.com"></input>
                            </div>
                        </div>

                        <div className="camera-type select-list">
                            <label>Access Type</label>
                            <div className="select-box">
                                <Select options={SelectList1} placeholder="Admin"  />
                            </div>
                        </div>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                            <Link className=" default-btn btn" to={"/add-your-team"}>
                                Cancel
                    </Link>
                            <a href="/add-your-team-table" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Add</a>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default NewUser;


