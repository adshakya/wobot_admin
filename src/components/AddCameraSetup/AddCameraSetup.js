import React from 'react';
import Select from 'react-select';
import { Link } from "react-router-dom";
import Models from './ModelTriggers';
import Footer from "../footer/footer";


const scaryAnimals = [
    { label: "Mask", value: 1 },
    { label: "Hairnet", value: 2 },
    { label: "Mopping", value: 3 },
    { label: "Person count", value: 4 },
    { label: "Attendance", value: 5 },
    { label: "Mask", value: 6 },
    { label: "Hairnet", value: 7 },
    { label: "Mopping", value: 8 },
    { label: "Person count", value: 9 },
    { label: "Attendance", value: 10 },
]

class AddCameraSetup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/cameras-table">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="heading"><h2>Add & Setup Camera</h2></div>
                        </div>
                        <div className="col-md-6">
                            <div className="camera-settings">
                                <Link className="" to={"/setting-camera"}>
                                    Connect
                       </Link>
                                <Link className="active" to={"/add-camera-setup"}>
                                    Camera settings
                       </Link>

                            </div>
                        </div>
                    </div>
                    <div className="main-content">
                            <p>Available Models in Triggers</p>
                        <ul className="camera-list">
                            <Models para="Detect Helmet" img={<img src={`${process.env.PUBLIC_URL}/assets/Helmet.png`} alt="" />} />
                            <Models para="Detect Handwash" img={<img src={`${process.env.PUBLIC_URL}/assets/outline.png`} alt="" />} />
                            <Models para="Detect Mobile" img={<img src={`${process.env.PUBLIC_URL}/assets/smartphone.png`} alt="" />} />
                            <Models para="Detect Helmet" img={<img src={`${process.env.PUBLIC_URL}/assets/Helmet.png`} alt="" />} />
                            <Models para="Detect Handwash" img={<img src={`${process.env.PUBLIC_URL}/assets/outline.png`} alt="" />} />
                            <Models para="Detect Mobile" img={<img src={`${process.env.PUBLIC_URL}/assets/smartphone.png`} alt="" />} />
                        </ul>
                       <div className="addmore-wrapper"> <div className="addmore"> Add 20 more
                                      <Select options={scaryAnimals} />
                            </div>
                            </div>
                        <div className="camera-setup">
                        <Link className="btn btn-primary btn-block btn-next" to={"/setting-camera-next"}>Camera Setup</Link>
                           
                        </div>
                       
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Setup to other cameras
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/add-new-camera"}>
                                Cancel
                    </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Save</button>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default AddCameraSetup;


