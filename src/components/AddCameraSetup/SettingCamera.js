import React from 'react';
import Select from 'react-select';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";


const SelectList1 = [
    { label: "DVR 1 (ID:123)", value: 1 },
    { label: "DVR 2 (ID:123)", value: 2 },
    { label: "DVR 3 (ID:123)", value: 3 },
    { label: "DVR 4 (ID:123)", value: 4 },
    { label: "DVR 5 (ID:123)", value: 5 },
]


const SelectList2 = [
    { label: "HSR Clinic Pantry 1", value: 1 },
    { label: "HSR Clinic Pantry 2", value: 2 },
    { label: "HSR Clinic Pantry 3", value: 3 },
    { label: "HSR Clinic Pantry 4", value: 4 },
    { label: "HSR Clinic Pantry 5", value: 5 },
]

const SelectList3 = [
    { label: "Cam 123", value: 1 },
    { label: "Cam 234", value: 2 },
    { label: "Cam 456", value: 3 },
    { label: "Cam 678", value: 4 },
]

const SelectList4 = [
    { label: "CP Plus", value: 1 },
    { label: "CP Plus2", value: 2 },
]

const SelectList5 = [
    { label: "12345", value: 1 },
    { label: "86583", value: 2 },
    { label: "35533", value: 3 },
    { label: "35355", value: 4 },
    { label: "12345", value: 5 },
]


class SettingCamera extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/setting-camera-next">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="heading"><h2>Add & Setup Camera</h2></div>
                        </div>
                        <div className="col-md-6">
                            <div className="camera-settings">
                                <Link className="active" to={"/setting-camera"}>
                                    Connect
                       </Link>
                                <Link className="" to={"/add-camera-setup"}>
                                    Camera settings
                       </Link>
                            </div>
                        </div>
                    </div>
                    <div className="main-content">
                        <div className="camera-type select-list">
                            <label>Camera Type</label>
                            <ul>
                                <li><Link className="active default-btn btn">DVR Connected</Link></li>
                                <li><Link className="default-btn btn">Standalone</Link></li>
                            </ul>
                        </div>

                        <div className="camera-type select-list">
                            <label>Select DVR</label>
                            <div className="select-box">
                                <Select options={SelectList1} />
                            </div>
                        </div>

                        <div className="camera-type select-list">
                            <label>Camera Name</label>
                            <div className="select-box">
                                <Select options={SelectList2} />
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <label>Camera ID</label>
                            <div className="select-box">
                                <Select options={SelectList3} />
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <label>Manufacturer</label>
                            <div className="select-box">
                                <Select options={SelectList4} />
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <label>Channel ID</label>
                            <div className="select-box">
                                <Select options={SelectList5} />

                            </div>
                            <Link className="default-btn connection-btn btn">Check Connection</Link>
                            <span className="succesful" aria-disabled>Able to connect succesfully</span>
                        </div>



                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                            <Link className=" default-btn btn" to={"/add-new-camera"}>
                                Cancel
                    </Link>
                            <a href="/add-camera-setup" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Next</a>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default SettingCamera;


