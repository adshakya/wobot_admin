import React from 'react';
import Select from 'react-select';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";



const SelectList1 = [
    { label: "Admin", value: 1 },
    { label: "Custom", value: 2 },
]


const SelectList2 = [
    { label: "Choose model", value: 1 },
    { label: "Mobile Phone", value: 2 },
    { label: "Helmet", value: 3 },
    { label: "Hair screen", value: 4 },
    { label: "Beard screen", value: 5 },
]

const SelectList3 = [
    { label: "Manager", value: 1 },
    { label: "Team Member", value: 2 },
]

const SelectList4 = [
    { label: "Yes", value: 1 },
    { label: "No", value: 2 },
]

const SelectList5 = [
    { label: "Issue ", value: 1 },
    { label: "Issue", value: 2 },
    { label: "Issue", value: 3 },
    { label: "Issue", value: 4 },
    { label: "Issue", value: 5 },
]


const SelectList6 = [
    { label: "Subissue", value: 1 },
    { label: "Subissue", value: 2 },
    { label: "Subissue", value: 3 },
    { label: "Subissue", value: 4 },
    { label: "Subissue", value: 5 },
]


class ExistingUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup settings-camera-page">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/setting-camera-next">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading"><h2>Add User</h2></div>
                            <div className="camera-type select-list select-modals">
                            <label>User Type</label>
                            <div className="select-box">
                            <ul>
                                <li><Link class=" default-btn btn" to={"/new-user"}>New User</Link></li>
                                <li><Link class="active default-btn btn" to={"/existing-user"}>Existing User</Link></li>
                            </ul>
                         </div>
                        </div>
                        </div>

                    </div>
                    <div className="main-content add-user-list">
                    <div className="camera-type select-list">
                            <label>Email address</label>
                            <div className="select-box">
                                <input type="text" name="" placeholder="email@team.com"></input>
                            </div>
                        </div>

                        <div className="camera-type select-list">
                            <label>Access Type</label>
                            <div className="select-box">
                                <Select options={SelectList1} placeholder="Admin"  />
                            </div>
                        </div>

                        <div className="location-type select-list">
                            <label className="location-title">Location</label>
                            <div className="select-box searchbar-input">
                              <input type="search" placeholder="Maps Autosearch"></input>

                              <div className="search-items">
                                  <label>India <span></span></label>
                                  <label>Delhi <span></span></label>
                                  <label>New Delhi <span></span></label>
                                  <label>Dwarka <span></span></label>
                              </div>
                            </div>
                        </div>
                        <div className="camera-type select-list select-modals">
                            <label>Models</label>
                            <div className="select-box">
                            <ul>
                                <li><Link class="active default-btn btn" href="">Item detection</Link></li>
                                <li><Link class="default-btn btn" href="">Activity detection</Link></li>
                            </ul>                           
                            </div>
                        </div>
                        <div className="camera-type select-list select-modals">
                            <label className="modal-title">Models</label>
                            <div className="select-box">
                            <Select options={SelectList2} placeholder="Choose Models" />
                            <div className="search-items">
                                  <label>Mobile Phone <span></span></label>
                                  <label>Hair screen <span></span></label>
                                  
                              </div>
                            </div>
                        </div>
                       <div className="moreinfo"> <label><Link href="#">More info</Link></label></div>
                       <div className="camera-type select-list">
                            <label>Contact number</label>
                            <div className="select-box">
                            <a className="telephone" href="tel:+4 (000) 000 00 00">+4 (000) 000 00 00</a>
                            </div>
                        </div>
                        <div className="camera-type select-list">
                           <div className="colum2">
                            <label>Designation</label>
                            <div className="select-box">
                                <Select options={SelectList3} placeholder="Manager" />
                            </div>
                            </div>
                            <div className="colum2 app-availability">
                            <label>App Availability</label>
                            <div className="select-box">
                                <Select options={SelectList4} placeholder="Yes"/>
                            </div>
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <label>Issue</label>
                            <div className="select-box">
                              <Select options={SelectList5} placeholder="Issue" />
                            </div>
                        </div>

                        <div className="camera-type select-list">
                            <label>Subissue</label>
                            <div className="select-box">
                              <Select options={SelectList6} placeholder="Subissue" />
                            </div>
                        </div>



                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                            <Link className=" default-btn btn" to={"/add-your-team"}>
                                Cancel
                    </Link>
                            <a href="/add-your-team-table" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Add</a>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default ExistingUser;


