import React, { Component } from "react";

export default class Footer extends Component {
    render() {
        return (
          <footer className="footer">
         <p>© 2019 Wobot Intelligence Pvt. Ltd. All Rights Reserved.</p>
          </footer>  
        );
    }
}
