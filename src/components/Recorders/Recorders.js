import React from 'react';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";

class Recorders extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup setup-process">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="heading"><h2>Recorders (DVR/NVR)</h2></div>
                    </div>
                </div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/add-recorders">
                    <div className="main-content">
                        <div className="row add-newdvr">
                            <div className="col-md-6"><h3>Add a new DVR</h3> <button className="btn btn-primary col-md-6 btn-block btn-next">Add New</button></div>
                            <div className="col-md-6 text-right"><a class="skip-btn btn default-btn " href="/recorders">Bulk button</a></div>
                        </div>

                        <p> Some DVR info Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim venia</p>

                        <div className="dont-recorder">
                            <Link class="default-btn " href="#">Don't have a recorder?</Link>
                        </div>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Skip entire setup
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                        <Link className=" default-btn btn" to={"/setup-process"}>
                          Back
                        </Link>
                            <button disabled type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Next</button>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default Recorders;


