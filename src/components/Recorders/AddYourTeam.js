import React from 'react';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";
import ReactTooltip from "react-tooltip";


class AddYourTeam extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup setup-process add-your-team-process">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/new-user">
                    <div className="main-content">
                        <div className="row add-newdvr">
                            <div className="col-md-6"><h3>Add your Team  
                                <span className="info-text" data-tip="Some info here">
                                <img width="20" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/info.png`} alt="" />
                                </span>
                                <ReactTooltip />
                                </h3> <button className="btn btn-primary col-md-6 btn-block btn-next">Add New</button></div>
                            <div className="col-md-6 text-right"><a class="skip-btn btn default-btn " href="/recorders">Bulk button</a></div>
                        </div>

                        <p> Details on how teams work info Lorem ipsum dolor sit amet,
                             consectetur adipiscing elit, sed do eiusmod tempor incididunt 
                             ut labore et dolore magna aliqua. Ut enim ad minim venia</p>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Skip entire setup
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                        <Link className=" default-btn btn" to={"/setup-completed"}>
                          Skip
                        </Link>
                        <Link className=" default-btn btn" to={"/cameras-table"}>
                          Back
                        </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Next</button>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default AddYourTeam;


