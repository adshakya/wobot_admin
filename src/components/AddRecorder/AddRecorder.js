import React from 'react';
import Select from 'react-select';
import { Link } from "react-router-dom";
import Footer from "../footer/footer";
import SearchBar from 'material-ui-search-bar'


const SelectList1 = [
    { label: "DVR 1", value: 1 },
    { label: "DVR 2", value: 2 },
    { label: "DVR 3", value: 3 },
    { label: "DVR 4", value: 4 },
    { label: "DVR 5", value: 5 },
]


const SelectList2 = [
    { label: "123", value: 1 },
    { label: "123", value: 2 },
    { label: "123", value: 3 },
]

const SelectList3 = [
    { label: "CP Plus ", value: 1 },
    { label: "CP Plus 2", value: 2 },
    { label: "CP Plus 3", value: 3 },
    { label: "CP Plus 4", value: 4 },
]

const SelectList4 = [
    { label: "Sub", value: 1 },
    { label: "Sub 2", value: 2 },
    { label: "Sub 3", value: 3 },
    { label: "Sub 4", value: 4 },
]

const Company = [
    { label: "Cult ", value: 1 },
    { label: "Cult 2", value: 2 },
    { label: "Cult 3", value: 3 },
    { label: "Cult 4", value: 4 },
]

const City = [
    { label: "Delhi ", value: 1 },
    { label: "Noida", value: 2 },
    { label: "Bombay", value: 3 },
    { label: "Pune", value: 4 },
]

const State = [
    { label: "New Delhi ", value: 1 },
    { label: "New Delhi", value: 2 },
    { label: "New Delhi", value: 3 },
    { label: "New Delhi", value: 4 },
]

const Center = [
    { label: "Dwarka ", value: 1 },
    { label: "Dwarka", value: 2 },
    { label: "Dwarka", value: 3 },
    { label: "Dwarka", value: 4 },
]


class AddRecorder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }
    render() {
        return (

            <div className="content add-camera-setup settings-camera-page add-recorder">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/recorders-table">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="heading"><h2>Add Recorder</h2></div>
                        </div>
                    </div>
                    <div className="main-content">
                        <div className="camera-type select-list">
                            <label>Name</label>
                            <div className="select-box">
                                <Select options={SelectList1} placeholder="DVR 1" />
                            </div>
                        </div>

                        <div className="camera-type select-list">
                            <label>DVR ID</label>
                            <div className="select-box">
                                <Select options={SelectList2} placeholder="123" />
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <label>Manufacturer</label>
                            <div className="select-box">
                                <Select options={SelectList3} placeholder="CP Plus" />
                            </div>
                        </div>
                        <div className="row input-field">
                            <div className="col-md-2">
                                <label>IP</label>
                                <input type="text" name="IP" />
                            </div>
                            <div className="col-md-2">
                                <label>User</label>
                                <input type="text" name="User" />
                            </div>
                            <div className="col-md-2">
                                <label>Password</label>
                                <input type="text" name="Password" />
                            </div>
                            <div className="col-md-2">
                                <label>Stream</label>
                                <Select options={SelectList4} placeholder="Sub" />
                            </div>
                        </div>
                        <div className="camera-type select-list">
                            <Link className="default-btn connection-btn btn">Check Connection</Link>
                            <span className="succesful"> <img width="20" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/check-marks.png`}
                                alt="sign" /> Connection Okay!</span>
                        </div>

                        <div className="row location-box">
                            <div className="col-md-2">
                                <h4>Location</h4>
                            </div>
                            <div className="col-md-4">
                                <SearchBar
                                    onChange={() => console.log('onChange')}
                                    onRequestSearch={() => console.log('onRequestSearch')}
                                    style={{
                                        margin: '0 auto',
                                        maxWidth: 300
                                    }}
                                />
                            </div>

                            <div className="col-md-4">
                                <Link className="default-btn btn">Autofill from Google Maps</Link>
                            </div>

                        </div>

                        <div className="row location">
                            <div className="camera-type select-list col-md-5">
                                <label>Company (L1)</label>
                                <div className="select-box">
                                    <Select options={Company} placeholder="Cult" />
                                </div>
                            </div>

                            <div className="camera-type select-list col-md-4">
                                <label>City (L2)</label>
                                <div className="select-box">
                                    <Select options={City} placeholder="Delhi" />
                                </div>
                            </div>
                        </div>


                        <div className="row location">
                            <div className="camera-type select-list col-md-5">
                                <label>State (L3)</label>
                                <div className="select-box">
                                    <Select options={State} placeholder="New Delhi" />
                                </div>
                            </div>

                            <div className="camera-type select-list col-md-4">
                                <label>Center (L4)</label>
                                <div className="select-box">
                                    <Select options={Center} placeholder="Dwarka" />
                                </div>
                            </div>
                        </div>

                        <div className="row location-box connected-cameras">
                            <div className="col-md-5">
                                <h4>Fetch Connected Cameras</h4>
                            </div>
                            <div className="col-md-5">
                                <div className="camera-type select-list">
                                    <div className="">
                                        <Link className="default-btn connection-btn btn"><img width="20" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/refresh.png`}
                                            alt="sign" /> Fetch Cameras</Link>
                                    </div>
                                    <span className="succesful"> <img width="20" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/check-marks.png`}
                                        alt="sign" /> 30 Cameras added.<br></br>
                                You can configure them in next step.</span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                            <Link className=" default-btn btn" to={"/recorders"}>
                                Cancel
                            </Link>
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Add</button>
                        </div>
                    </div>

                </form>
                <Footer />
            </div>
        );
    }
}
export default AddRecorder;


