import React from 'react';
import "mdbreact/dist/css/mdb.css";
import { Link } from "react-router-dom";
import Footer from "../footer/footer";
import ReactTooltip from "react-tooltip";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faInfo } from '@fortawesome/free-solid-svg-icons'



const RecorderTable = () => {
    return (
        <div>
            <div className="logo"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                alt="logo" /></div>

            <div className="content add-camera-setup settings-camera-page add-recorder recorder-table">
                <div className="row">
                    <div className="col-md-12">
                        <div className="heading text-center"><h2>Recorders (DVR/NVR)</h2></div>
                    </div>
                </div>


                <div className="white-box">
                    <div className="row">
                        <div className="col-md-12 text-right">
                        <a href="/add-recorders"className="btn btn-primary col-md-6 btn-block btn-next">Add New</a>
                            <a class="skip-btn btn default-btn btn-bulk" href="/setup-process">Bulk Import</a>
                        </div>
                    </div>

                    <div className="row table-box">
                        <table class="table table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>DVR ID </th>
                                    <th>Mfg</th>
                                    <th>IP</th>
                                    <th>STATUS</th>
                                    <th>Cameras</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>DVR 1</td>
                                    <td>CP Plus</td>
                                    <td>1234</td>
                                    <td>Online</td>
                                    <td>30</td>
                                    <td className="icons">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            <FontAwesomeIcon icon={faInfo} />
                                        </span>
                                        <span data-tip="Edit">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </span>
                                        <ReactTooltip />
                                    </td>
                                </tr>
                                <tr>
                                    <td>DVR 1</td>
                                    <td>CP Plus</td>
                                    <td>1234</td>
                                    <td>Online</td>
                                    <td>30</td>
                                    <td className="icons">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            <FontAwesomeIcon icon={faInfo} />
                                        </span>
                                        <span data-tip="Edit">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>DVR 1</td>
                                    <td>CP Plus</td>
                                    <td>1234</td>
                                    <td>Online</td>
                                    <td>30</td>
                                    <td className="icons">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            <FontAwesomeIcon icon={faInfo} />
                                        </span>
                                        <span data-tip="Edit">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn " to={"#"}>
                                Skip entire setup
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <Link className=" default-btn btn" to={"/add-recorders"}>
                                Back
                            </Link>
                            <a href="/add-new-camera" type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Next</a>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </div>
    );
}
export default RecorderTable;


