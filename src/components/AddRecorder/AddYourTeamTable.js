import React from 'react';
import "mdbreact/dist/css/mdb.css";
import { Link } from "react-router-dom";
import Footer from "../footer/footer";
import ReactTooltip from "react-tooltip";



const AddYourTeamTable = () => {
    return (
        <div>
            <div className="logo"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                alt="logo" /></div>

            <div className="content add-camera-setup settings-camera-page add-recorder recorder-table">
                <div className="row">
                    <div className="col-md-12">
                        <div className="heading text-center"><h2>Add Your Team</h2></div>
                    </div>
                </div>


                <div className="white-box">
                    <div className="row">
                        <div className="col-md-12">
                        <div class="camera-settings">
                            <a class="" href="/recorders-table">Recorders</a>
                            <a class="" href="/cameras-table">Cameras</a>
                            <a class="active" href="/add-your-team-table">Users</a>
                        </div>
                        </div>
                        <div className="col-md-12 text-right">
                        <a href="/new-user"className="btn btn-primary col-md-6 btn-block btn-next">Add New</a>
                            <a class="skip-btn btn default-btn btn-bulk" href="/setup-process">Bulk Import</a>
                        </div>
                    </div>

                    <div className="row table-box add-user-team-info">
                        <table class="table table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th>Group</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>abc@doe.com</td>
                                    <td>+4 (000) 000 00 00</td>
                                    <td>Admin</td>
                                    <td>Active since 16th May 2020</td>
                                    <td className="icons add-user-info">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            Country: India<br></br> 
                                            Categoty: Clinic                                      
                                      </span>
                                       
                                        <ReactTooltip />
                                    </td>
                                </tr>
                                <tr>
                                    <td>abc@doe.com</td>
                                    <td>+4 (000) 000 00 00</td>
                                    <td>Admin</td>
                                    <td>Active since 16th May 2020</td>
                                    <td className="icons add-user-info">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            Country: India<br></br> 
                                            Categoty: Clinic                                      
                                      </span>
                                       
                                        <ReactTooltip />
                                    </td>
                                </tr>
                                <tr>
                                    <td>abc@doe.com</td>
                                    <td>+4 (000) 000 00 00</td>
                                    <td>Admin</td>
                                    <td>Inactive <Link href="#">Resend invite</Link></td>
                                    <td className="icons add-user-info">
                                        <span className="info" data-tip="Location:    
                                            Cult, Delhi, New Delhi, Dwarka">
                                            Country: India<br></br> 
                                            Categoty: Clinic                                      
                                      </span>
                                       
                                        <ReactTooltip />
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                        <Link className=" default-btn btn" to={"/cameras-table"}>
                          Back
                        </Link>
                        <Link to={"/setup-completed"} className="btn btn-primary col-md-6 btn-block btn-next">Next</Link>
                        </div>
                    </div>
                   
                </div>
                <Footer />
            </div>
        </div>
    );
}
export default AddYourTeamTable;


