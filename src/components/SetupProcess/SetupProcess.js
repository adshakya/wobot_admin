import React from 'react';
import { Link } from "react-router-dom";
import Models from './../AddCameraSetup/ModelTriggers';
import Footer from "../footer/footer";
import Popup from "reactjs-popup";


class SetupProcess extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);

    }

    openModal() {
        this.setState({ open: true });
    }
    closeModal() {
        this.setState({ open: false });
    }
    render() {
        return (
            <div className="content add-camera-setup setup-process">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="heading"><h2>Starting Setup Process</h2></div>
                    </div>
                </div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/recorders">
                    <div className="main-content">
                        <p> The next steps will allow you to quickly configure your account.
                        It comprises of three steps. For the setup to complete successfully,
                            please have the below information handy:</p>
                        <ul>
                            <li><span>DVR Access Credentials</span></li>
                            <li><span>Camera details and IP</span></li>
                            <li><span>Your team email addresses</span></li>
                            <li><span>Grouping hierarchy</span></li>
                        </ul>
                        <ul className="camera-list">
                            <Models para="Setup DVR" img={<img src={`${process.env.PUBLIC_URL}/assets/rec.png`} alt="" height="59" />} />
                            <Models para="Add Cameras" img={<img src={`${process.env.PUBLIC_URL}/assets/cam.png`} alt="" height="59" />} />
                            <Models para="Add team" img={<img src={`${process.env.PUBLIC_URL}/assets/team.png`} alt="" height="59" />} />

                        </ul>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-6">
                            <Link className="skip-btn btn default-btn" onClick={this.openModal} to={"#"}>
                                Skip entire setup
                    </Link>
                        </div>
                        <div className="col-md-6 text-right">
                            <button type="submit" className="btn btn-primary col-md-6 btn-block btn-next">Continue</button>
                        </div>
                    </div>

                </form>
                <Footer />

                <div className="skip-content">
                <Popup
                    open={this.state.open}
                    closeOnDocumentClick
                    onClose={this.closeModal}
                >
                    <div className="modal-content">
                        {/* <span className="close" onClick={this.closeModal}> &times;</span> */}
                        <h3>Skip setup?</h3>
                        <div className="row">
                            <div className="col-md-12">
                                <div class="">
                                    <p>If you skip now, you will have to manually configure your account</p>
                                    
                                    <div className="row bottom-btns">
                                            <div className="col-md-12 text-right">
                                               <Link onClick={this.closeModal} type="submit" className="btn btn-primary col-md-6 btn-block btn-next">No</Link>
                                                <Link disabled className=" default-btn btn" to={"/setting-camera"}>
                                                Yes
                                        </Link>
                                            </div>
                                        </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </Popup>
                </div>
            </div>
        );
    }
}
export default SetupProcess;


