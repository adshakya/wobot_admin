import React from 'react';
import Footer from "../footer/footer";
import { Link } from "react-router-dom";

class SetupCompleted extends React.Component {

    render() {
        return (
            <div className="content add-camera-setup setup-process setup-page-completed">
                <div className="logo fixed"><img width="50" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/logo.svg`}
                    alt="logo" /></div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="heading"><h2>Setup Completed</h2></div>
                    </div>
                </div>
                <form className="white-box" onSubmit={this.handleSubmit} action="/recorders">
                    <div className="main-content">
                        <p> <img width="40" className="img-fluid" src={`${process.env.PUBLIC_URL}/assets/check-marks.png`}
                alt="logo" /> You are all set</p>
                    </div>

                    <div className="row bottom-btns">
                        <div className="col-md-12 text-right">
                          <Link to={"/setup-completed"} className="btn btn-primary col-md-6 btn-block btn-next">Completed</Link>
                        </div>
                    </div>

                </form>
                <Footer />

            </div>
        );
    }
}
export default SetupCompleted;


